let config = require("./config.js");
let Binance = require("binance-api-node").default;
// Authenticated client, can make signed calls
const client = Binance({
  apiKey: config.binance.key,
  apiSecret: config.binance.secret,
});

let orderSize = 0.2;

async function getPrice(ins) {
  return await client.avgPrice({ symbol: ins["Symbol"] });
}

async function getAccount(ins) {
  return await client.accountInfo();
}

async function getSize(ins) {
  let alt = ins["Symbol"].replace("BTC", "");
  let account = await getAccount();
  let balance = account.balances.filter((x) => x.asset === alt)[0].free;
  if (balance === 0) {
    let balance = acccount.balances.filter((x) => x.asset === "BTC")[0].free;
    let getPrice = await getPrice(ins);
    let price = getPrice.price;
    let size = balance * price * orderSize;
    return size;
  } else {
    return balance;
  }
}

async function newOrder(ins) {
  let size = await getSize(ins);
  return await client.order({
    symbol: ins["Symbol"],
    side: ins["Side"],
    quantity: size,
    price: ins["Price"],
    type: ins["Order Type"],
    timeInForce: ins["Time In Force"],
    newClientOrderId: ins["Order Id"],
  });
}

async function deleteOrder(ins) {
  return await client.cancelOrder({
    symbol: ins["Symbol"],
    origClientOrderId: ins["Order ID"],
  });
}

async function execute(command, instructions) {
  console.log(command);
  if (command === "New") {
    let res = await newOrder(instructions);
    console.log(res);
    return `
    **New Order**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
  if (command === "Cancel") {
    let res = await deleteOrder(instructions);
    console.log(res);
    return `
    **Cancel Order**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
  if (command === "GetAccount") {
    let res = await getAccount();
    console.log(res);
    return `
    **Get Account**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
  if (command === "GetPrice") {
    let res = await getPrice(instructions);
    console.log(res);
    return `
    **Get Price**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
}

module.exports = execute;
