let crypto = require("crypto");
let axios = require("axios");
let config = require("./config.js");

let key = config.phemex.key;
let secret = config.phemex.secret;

let base = "https://api.phemex.com";
let orderSize = 0.5;

let newOrderSize = {
  "5": 0.9,
  "10": 0.9,
  "15": 0.66,
  "20": 0.5,
  "25": 0.4,
  "30": 0.33,
  "50": 0.2,
  "75": 0.0133,
  "100": 0.01,
};

async function apiPOST(url, data) {
  let expiry = Math.round(Date.now() / 1000 + 60);
  let signature = crypto
    .createHmac("sha256", secret)
    .update(url + expiry + JSON.stringify(data))
    .digest("hex");
  let headers = {
    "x-phemex-access-token": key,
    "x-phemex-request-expiry": expiry,
    "x-phemex-request-signature": signature,
  };
  try {
    return await axios.post(base + url, data, {
      headers: {
        ...headers,
      },
    });
  } catch (err) {
    console.log(err);
  }
}

async function apiPUT(url, data) {
  let expiry = Math.round(Date.now() / 1000 + 60);
  let query = Object.keys(data)
    .map((key) => key + "=" + data[key])
    .join("&");
  let signature = crypto
    .createHmac("sha256", secret)
    .update(url + query + expiry)
    .digest("hex");
  let headers = {
    "x-phemex-access-token": key,
    "x-phemex-request-expiry": expiry,
    "x-phemex-request-signature": signature,
  };
  try {
    return await axios.put(
      base + url + "?" + query,
      {},
      {
        headers: {
          ...headers,
        },
      }
    );
  } catch (err) {
    console.log(err);
  }
}

async function apiDELETE(url, data) {
  let expiry = Math.round(Date.now() / 1000 + 60);
  let query = Object.keys(data)
    .map((key) => key + "=" + data[key])
    .join("&");
  let signature = crypto
    .createHmac("sha256", secret)
    .update(url + query + expiry)
    .digest("hex");
  let headers = {
    "x-phemex-access-token": key,
    "x-phemex-request-expiry": expiry,
    "x-phemex-request-signature": signature,
  };
  try {
    return await axios.delete(base + url + "?" + query, {
      headers: {
        ...headers,
      },
    });
  } catch (err) {
    console.log(err);
  }
}

async function apiGET(url, data) {
  let query = Object.keys(data)
    .map((key) => key + "=" + data[key])
    .join("&");
  let expiry = Math.round(Date.now() / 1000 + 60);
  let signature = crypto
    .createHmac("sha256", secret)
    .update(url + query + expiry)
    .digest("hex");
  let headers = {
    "x-phemex-access-token": key,
    "x-phemex-request-expiry": expiry,
    "x-phemex-request-signature": signature,
  };
  try {
    return await axios.get(base + url + "?" + query, {
      headers: {
        ...headers,
      },
    });
  } catch (err) {
    console.log(err);
  }
}

async function getPrice(ins) {
  let data = {
    // symbol: ins["Symbol"],
    symbol: "BTCUSD",
  };
  return await apiGET("/md/orderbook", data);
}

async function getOrder(ins) {
  let data = {
    symbol: ins["Symbol"],
    clOrdID: ins["Order ID"],
  };
  return await apiGET("/exchange/order", data);
}

async function getAccount() {
  let data = {};

  return await apiGET("/phemex-user/users/children", data);
}

async function getSize(ins) {
  let account = await getAccount();
  let leverage = ins["Leverage"] / 100000000;
  let balance = account.data.data[0].userMarginVo;
  let symbol = ins["Symbol"];
  if (symbol === "BTCUSD") {
    let balanceBTC = balance[0].accountBalance;
    let orderBook = await getPrice(ins);
    let askPrice = orderBook.data.result.book.asks[0][0];
    let price = askPrice / 10000;
    let size = Math.round(
      price * balanceBTC * leverage * newOrderSize[leverage]
    );
    console.log(
      symbol,
      size,
      price,
      balanceBTC,
      leverage,
      newOrderSize[leverage]
    );
    return size;
  } else {
    let balanceUSD = balance[1].accountBalance;
    let size = Math.round(balanceUSD * leverage * newOrderSize[leverage]);
    console.log(symbol, size, balanceUSD, leverage, newOrderSize[leverage]);
    return size;
  }
}

async function getPositions(ins) {
  let symbol = ins["Symbol"];
  let currency = symbol === "BTCUSD" ? "BTC" : "USD";
  let data = {
    currency: currency,
  };
  let positions = await apiGET("/accounts/accountPositions", data);
  let position = positions.data.data.positions.filter(
    (x) => x.symbol === symbol
  );
  let size = 0;
  if (position.length > 0) {
    size = position[0].size;
  }
  return size;
}

async function newOrder(ins) {
  let data = {};
  if (["Market", "Limit"].includes(ins["Order Type"])) {
    await changeLeverage(ins);
    let positionSize = await getPositions(ins);
    let size = positionSize === 0 ? await getSize(ins) : positionSize;
    data = {
      actionBy: ins["Action By"],
      symbol: ins["Symbol"],
      clOrdID: ins["Order ID"],
      side: ins["Side"],
      priceEp: ins["Price Ep"],
      orderQty: size,
      ordType: "Market",
      triggerType: ins["Trigger"],
      timeInForce: ins["Time In Force"],
    };
  } else {
    data = {
      actionBy: ins["Action By"],
      symbol: ins["Symbol"],
      clOrdID: ins["Order ID"],
      side: ins["Side"],
      priceEp: ins["Price Ep"],
      orderQty: 0,
      ordType: ins["Order Type"],
      triggerType: ins["Trigger"],
      timeInForce: ins["Time In Force"],
      takeProfitEp: ins["Take Profit Ep"],
      stopLossEp: ins["Stop Loss Ep"],
      stopPxEp: ins["Stop Px Ep"],
      pegOffsetValueEp: ins["Peg Offset Value Ep"],
    };
  }
  return await apiPOST("/orders", data);
}

async function ammendOrder(ins) {
  let res = await getOrder(ins);
  // let order = res.data.data.filter(
  //   (x) => x.ordStatus === "New" || x.ordStatus === "Untriggered"
  // );
  let order = res.data.data;
  if (order.length !== 0) {
    order = order[0];
  } else {
    return false;
  }

  let data = {
    actionBy: ins["Action By"],
    symbol: ins["Symbol"],
    orderID: order.orderID,
    priceEp: ins["Price Ep"],
    orderQty: ins["Coef Size"],
    takeProfitEp: ins["Take Profit Ep"],
    stopLossEp: ins["Stop Loss Ep"],
    stopPxEp: ins["Stop Px Ep"],
    pegOffsetValueEp: ins["Peg Offset Value Ep"],
  };
  console.log(data);
  return await apiPUT("/orders/replace", data);
}

async function deleteOrder(ins) {
  let res = await getOrder(ins);
  // let order = res.data.data.filter(
  //   (x) => x.ordStatus === "New" || x.ordStatus === "Untriggered"
  // );
  let order = res.data.data;
  if (order.length !== 0) {
    order = order[0];
  } else {
    return false;
  }

  let data = {
    symbol: ins["Symbol"],
    orderID: order.orderID,
  };
  console.log(data);
  return await apiDELETE("/orders/cancel", data);
}

async function changeLeverage(ins) {
  let data = {
    symbol: ins["Symbol"],
    leverageEr: ins["Leverage"],
  };
  return await apiPUT("/positions/leverage", data);
}

async function execute(command, instructions) {
  console.log(command);
  if (command === "New") {
    let res = await newOrder(instructions);
    console.log(res);
    return `
    **New Order**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
  if (command === "Replace") {
    let res = await ammendOrder(instructions);
    console.log(res);
    return `
    **Replace Order**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
  if (command === "Cancel") {
    let res = await deleteOrder(instructions);
    console.log(res);
    return `
    **Cancel Order**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
  if (command === "SetLeverage") {
    let res = await changeLeverage(instructions);
    console.log(res);
    return `
    **Set Leverage**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
  if (command === "GetAccount") {
    let res = await getAccount(instructions);
    console.log(res);
    return `
    **Get Account**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
  if (command === "GetOrder") {
    let res = await getOrder(instructions);
    console.log(res);
    return `
    **Get Order**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
  if (command === "GetPrice") {
    let res = await getPrice(instructions);
    console.log(res);
    return `
    **Get Price**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
  if (command === "GetPositions") {
    let res = await getPositions(instructions);
    console.log(res);
    return `
    **Get Price**
    ---
    ${JSON.stringify(res.data)}
    `;
  }
}

module.exports = execute;
