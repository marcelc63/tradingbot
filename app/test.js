**New**
```json
{
    "Action By": "ByUser",
    "Coef Size": "7",
    "Exec Inst": "CloseOnTrigger",
    "Leverage": 5000000000,
    "Msg ID": "a1e55bbb-174c-4cab-af06-775e9fed97e2",
    "Order ID": "10fbe917-0295-4ad1-90ce-c14c892ad971",
    "Order Type": "Limit",
    "Peg Offset Value Ep": 0,
    "Price Ep": 75630000,
    "Side": "Sell",
    "Stop Loss Ep": 0,
    "Stop Px Ep": 0,
    "Symbol": "BTCUSD",
    "Take Profit Ep": 0,
    "Term": 46,
    "Time In Force": "GoodTillCancel",
    "Trigger": ""
}
```

**New**
```json
{    
    "Action By": "FromOrderPlacement",
    "Coef Size": "0",
    "Exec Inst": "CloseOnTrigger",
    "Leverage": 5000000000,
    "Msg ID": "f4c3197a-09c3-4aa3-a9c4-00c78c86bd8a",
    "Order ID": "16034365-8ac7-49dd-b10d-76f04ed46860",
    "Order Type": "MarketIfTouched",
    "Peg Offset Value Ep": 0,
    "Price Ep": 0,
    "Side": "Sell",
    "Stop Loss Ep": 0,
    "Stop Px Ep": 69000000,
    "Symbol": "BTCUSD",
    "Take Profit Ep": 0,
    "Term": 45,
    "Time In Force": "ImmediateOrCancel",
    "Trigger": "ByLastPrice"
}
```

**New**
```json
{    
    "Action By": "FromOrderPlacement",
    "Coef Size": "0",
    "Exec Inst": "CloseOnTrigger",
    "Leverage": 5000000000,
    "Msg ID": "f4c3197a-09c3-4aa3-a9c4-00c78c86bd8a",
    "Order ID": "16034365-8ac7-49dd-b10d-76f04ed46860",
    "Order Type": "Stop",
    "Peg Offset Value Ep": 0,
    "Price Ep": 0,
    "Side": "Sell",
    "Stop Loss Ep": 0,
    "Stop Px Ep": 66000000,
    "Symbol": "BTCUSD",
    "Take Profit Ep": 0,
    "Term": 45,
    "Time In Force": "ImmediateOrCancel",
    "Trigger": "ByLastPrice"
}
```

**Replace**
```json
{
    "Action By": "ByUser",
    "Coef Size": "7",
    "Exec Inst": "CloseOnTrigger",
    "Leverage": 5000000000,
    "Msg ID": "a1e55bbb-174c-4cab-af06-775e9fed97e2",
    "Order ID": "10fbe917-0295-4ad1-90ce-c14c892ad971",
    "Order Type": "Limit",
    "Peg Offset Value Ep": 0,
    "Price Ep": 76630000,
    "Side": "Sell",
    "Stop Loss Ep": 0,
    "Stop Px Ep": 0,
    "Symbol": "BTCUSD",
    "Take Profit Ep": 0,
    "Term": 46,
    "Time In Force": "GoodTillCancel",
    "Trigger": ""
}
```

**Cancel**
```json
{
    "Msg ID": "1f3391db-4c39-49e2-a9e9-ca649fccf1cd",
    "Order ID": "10fbe917-0295-4ad1-90ce-c14c892ad971",
    "Symbol": "BTCUSD"
}
```

**SetLeverages**
```json
{
    "Leverage": 5000000000,
    "Msg ID": "d99b9076-dd36-4074-b4c9-f97f19c0f437",
    "Symbol": "BTCUSD"
}
```

**GetAccount**
```json
{
    "Currency": "BTC"
}    
```

**GetOrder**
```json
{
    "Symbol": "BTCUSD",
    "Order ID": "45f3bb92-45e4-4ca3-af78-2f90332ebca2"
}
```

**GetPrice**
```json
{
    "Symbol": "LINKUSD"
}
```


**New**
```json
{
    "Action By": "ByUser",
    "Coef Size": "21675",
    "Exec Inst": "",
    "Leverage": 2500000000,
    "Msg ID": "b464ab90-4104-4711-b8f5-c87c7df975af",
    "Order ID": "7a1d1fa6-6af5-408a-89b4-aa3f85654b52",
    "Order Type": "Market",
    "Peg Offset Value Ep": 0,
    "Price Ep": 87855000,
    "Side": "Sell",
    "Stop Loss Ep": 88385000,
    "Stop Px Ep": 0,
    "Symbol": "BTCUSD",
    "Take Profit Ep": 86985000,
    "Term": 81,
    "Time In Force": "ImmediateOrCancel",
    "Trigger": ""
}
```

**New**
```json
{
    "Action By": "FromOrderPlacement",
    "Coef Size": "0",
    "Exec Inst": "CloseOnTrigger",
    "Leverage": 2500000000,
    "Msg ID": "67c6a71b-aa58-42ba-b808-db02098f0eae",
    "Order ID": "bc7c7a56-d490-41ce-8b44-db9ad4ba4ff2",
    "Order Type": "MarketIfTouched",
    "Peg Offset Value Ep": 0,
    "Price Ep": 0,
    "Side": "Buy",
    "Stop Loss Ep": 0,
    "Stop Px Ep": 86985000,
    "Symbol": "BTCUSD",
    "Take Profit Ep": 0,
    "Term": 81,
    "Time In Force": "ImmediateOrCancel",
    "Trigger": "ByLastPrice"
}
```

**New**
```json
{
    "Action By": "FromOrderPlacement",
    "Coef Size": "0",
    "Exec Inst": "CloseOnTrigger",
    "Leverage": 2500000000,
    "Msg ID": "9ce7cdaf-40ac-44f0-9cd1-94ad717eda02",
    "Order ID": "45f3bb92-45e4-4ca3-af78-2f90332ebca2",
    "Order Type": "Stop",
    "Peg Offset Value Ep": 0,
    "Price Ep": 0,
    "Side": "Buy",
    "Stop Loss Ep": 0,
    "Stop Px Ep": 88385000,
    "Symbol": "BTCUSD",
    "Take Profit Ep": 0,
    "Term": 81,
    "Time In Force": "ImmediateOrCancel",
    "Trigger": "ByMarkPrice"
}
```

**Replace**
```json
{
    "Action By": "FromOrderPlacement",
    "Coef Size": "0",
    "Exec Inst": "CloseOnTrigger",
    "Leverage": 2500000000,
    "Msg ID": "783310b1-6377-4a79-abfa-cb247875ae9a",
    "Order ID": "45f3bb92-45e4-4ca3-af78-2f90332ebca2",
    "Order Type": "Stop",
    "Peg Offset Value Ep": 0,
    "Price Ep": 0,
    "Side": "Buy",
    "Stop Loss Ep": 0,
    "Stop Px Ep": 88660000,
    "Symbol": "BTCUSD",
    "Take Profit Ep": 0,
    "Term": 81,
    "Time In Force": "ImmediateOrCancel",
    "Trigger": "ByMarkPrice"
}
```

**GetPositions**
```json
{
    "Symbol": "BTCUSD"
}    
```

**New**
```json
{
    "Action By": "ByUser",
    "Coef Size": "21675",
    "Exec Inst": "CloseOnTrigger",
    "Leverage": 2500000000,
    "Msg ID": "795a26fa-0331-4d3e-83b0-39a3e6618c3e",
    "Order ID": "5d063d88-2df7-4bde-83b8-4e589d21c538",
    "Order Type": "Market",
    "Peg Offset Value Ep": 0,
    "Price Ep": 91530000,
    "Side": "Buy",
    "Stop Loss Ep": 0,
    "Stop Px Ep": 0,
    "Symbol": "BTCUSD",
    "Take Profit Ep": 0,
    "Term": 81,
    "Time In Force": "ImmediateOrCancel",
    "Trigger": ""
}
```

**New**
```json
{
    "Action By": "FromOrderPlacement",
    "Coef Size": "0",
    "Exec Inst": "CloseOnTrigger",
    "Leverage": 500000000,
    "Msg ID": "ee968d72-e165-45f0-87dd-0fc21fec6b6d",
    "Order ID": "ffab8acb-e242-4a38-b53d-f0d2d34ac23b",
    "Order Type": "Stop",
    "Peg Offset Value Ep": 0,
    "Price Ep": 0,
    "Side": "Sell",
    "Stop Loss Ep": 0,
    "Stop Px Ep": 2033500,
    "Symbol": "ETHUSD",
    "Take Profit Ep": 0,
    "Term": 22,
    "Time In Force": "ImmediateOrCancel",
    "Trigger": "ByMarkPrice"
}
```
**New**
```json
{
    "Action By": "FromOrderPlacement",
    "Coef Size": "0",
    "Exec Inst": "CloseOnTrigger",
    "Leverage": 500000000,
    "Msg ID": "938397cb-0504-4d7a-9ccb-9fb570350033",
    "Order ID": "53023fa4-d741-487a-98c8-4cfafc8a3322",
    "Order Type": "MarketIfTouched",
    "Peg Offset Value Ep": 0,
    "Price Ep": 0,
    "Side": "Sell",
    "Stop Loss Ep": 0,
    "Stop Px Ep": 2306000,
    "Symbol": "ETHUSD",
    "Take Profit Ep": 0,
    "Term": 22,
    "Time In Force": "ImmediateOrCancel",
    "Trigger": "ByLastPrice"
}
```

**Replace**
```json
{
    "Action By": "FromOrderPlacement",
    "Coef Size": "0",
    "Exec Inst": "CloseOnTrigger",
    "Leverage": 500000000,
    "Msg ID": "e110e222-acd4-4de8-8315-09ad8362d859",
    "Order ID": "ffab8acb-e242-4a38-b53d-f0d2d34ac23b",
    "Order Type": "Stop",
    "Peg Offset Value Ep": 0,
    "Price Ep": 0,
    "Side": "Sell",
    "Stop Loss Ep": 0,
    "Stop Px Ep": 2016500,
    "Symbol": "ETHUSD",
    "Take Profit Ep": 0,
    "Term": 22,
    "Time In Force": "ImmediateOrCancel",
    "Trigger": "ByMarkPrice"
}
```